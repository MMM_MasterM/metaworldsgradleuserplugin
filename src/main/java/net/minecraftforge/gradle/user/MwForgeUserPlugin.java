package net.minecraftforge.gradle.user;

import java.util.HashMap;

import static net.minecraftforge.gradle.user.UserConstants.*;
import static net.minecraftforge.gradle.user.MwUserConstants.*;
import net.minecraftforge.gradle.delayed.DelayedFile;
import net.minecraftforge.gradle.delayed.DelayedFileTree;
import net.minecraftforge.gradle.delayed.DelayedString;
import net.minecraftforge.gradle.delayed.DelayedBase.IDelayedResolver;
import net.minecraftforge.gradle.user.UserExtension;
import net.minecraftforge.gradle.tasks.abstractutil.ExtractTask;
import net.minecraftforge.gradle.tasks.user.ApplyBinPatchesTask;

import org.gradle.api.DefaultTask;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;

public class MwForgeUserPlugin implements Plugin<Project>, IDelayedResolver<UserExtension> {
	public Project    project;
    
    static final String metaworldsDir = "metaworlds";
    
    @Override
    public final void apply(Project arg)
    {
    	project = arg;
    	
    	applyPlugin();
    }
    
    public void applyPlugin()
    {
    	ExtractTask extractMwUserDev = makeTask("extractMwUserDev", ExtractTask.class);
    	{
    		extractMwUserDev.from(delayedFile(MW_CORE_JAR));
    		extractMwUserDev.into(delayedFile(MW_PACK_DIR));
    	}
    	
    	project.getTasks().getByName("extractUserDev").dependsOn("extractMwUserDev");
    	
    	ApplyBinPatchesTask task = (ApplyBinPatchesTask)project.getTasks().getByName("applyBinPatches");
    	{
    		task.setOutJar(delayedFile(MW_FORGE_BINPATCHED));
    	}
    	
    	task = makeTask("applyBinPatchesMw", ApplyBinPatchesTask.class);
    	{
    		task.setInJar(delayedFile(MW_FORGE_BINPATCHED));
    		task.setOutJar(delayedFile(FORGE_CACHE + FORGE_BINPATCHED));
    		task.setPatches(delayedFile(MW_BINPATCHES));
    		task.setClassesJar(delayedFile(MW_BINARIES_JAR));
    		task.setResources(delayedFileTree(MW_RES_DIR));
    		task.dependsOn("applyBinPatches");
    	}
    	
    	project.getTasks().getByName("deobfBinJar").dependsOn("applyBinPatchesMw");
    }
    
    protected DelayedString delayedString(String path)
    {
        return new DelayedString(project, path, this);
    }
    
    protected DelayedFile delayedFile(String path)
    {
        return new DelayedFile(project, path, this);
    }
    
    protected DelayedFileTree delayedFileTree(String path)
    {
        return new DelayedFileTree(project, path, this);
    }
    
    protected DelayedFileTree delayedZipTree(String path)
    {
        return new DelayedFileTree(project, path, true, this);
    }

    public DefaultTask makeTask(String name)
    {
        return makeTask(name, DefaultTask.class);
    }

    public <T extends Task> T makeTask(String name, Class<T> type)
    {
        return makeTask(project, name, type);
    }

    @SuppressWarnings("unchecked")
    public static <T extends Task> T makeTask(Project proj, String name, Class<T> type)
    {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("name", name);
        map.put("type", type);
        return (T) proj.task(map, name);
    }

	@Override
	public String resolve(String pattern, Project arg1, UserExtension exten) {
		/*if (version != null)
            pattern = pattern.replace("{ASSET_INDEX}", version.getAssets());*/
		
		pattern = pattern.replace("{API_VERSION}", exten.getApiVersion());
        return pattern;
	}
}
