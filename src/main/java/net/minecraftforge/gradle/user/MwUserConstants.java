package net.minecraftforge.gradle.user;

import static net.minecraftforge.gradle.user.UserConstants.*;

final class MwUserConstants {
	static final String MW_FORGE_BINPATCHED 			= "{BUILD_DIR}/mwTmp/minecraft_patched.zip";
	
	static final String MW_CORE_JAR						= "metaworlds.jar";
	
	static final String MW_PACK_DIR						= "{BUILD_DIR}/unpacked_mw";
	static final String MW_BINPATCHES					= MW_PACK_DIR + "/devbinpatches.pack.lzma";
	
	static final String MW_BINARIES_JAR					= MW_PACK_DIR + "/binaries.jar";
	static final String MW_RES_DIR						= MW_PACK_DIR + "/src/main/resources";
}
